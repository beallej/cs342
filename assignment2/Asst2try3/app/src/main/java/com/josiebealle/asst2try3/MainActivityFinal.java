package com.josiebealle.asst2try3;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


public class MainActivityFinal extends ListActivity {
    public static final int WEB_REQUEST_CODE = 2;
    public static final int SB_REQUEST_CODE = 4;
    public static final int CALC_REQUEST_CODE = 9;
    public static final int AGE_REQUEST_CODE = 8;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity_final);

        ArrayList<String> listItems = new ArrayList<String>();
        listItems.add("Calculator");
        listItems.add("Web Game!");
        listItems.add("Age Activity");
        listItems.add("Soundboardt");

        final ListDemoAdapter adapter = new ListDemoAdapter(this, R.layout.list_cell, listItems);
        ListView listView = (ListView) this.findViewById(android.R.id.list);
        listView.setAdapter(adapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_main_activity_final, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListItemClick(ListView parent, View v, int position, long id) {


        TextView textView = (TextView)v.findViewById(R.id.listCellTextView);
        if (v != null && textView != null) {
            if (position == 0) {
                Intent intent = new Intent(MainActivityFinal.this, CalculatorActivity.class);
                startActivityForResult(intent, MainActivityFinal.CALC_REQUEST_CODE);
                overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            }
            if (position == 1) {
                Intent intent = new Intent(MainActivityFinal.this, WebActivity.class);
                startActivityForResult(intent, MainActivityFinal.WEB_REQUEST_CODE);
                overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            }
            if (position == 2) {
                Intent intent = new Intent(MainActivityFinal.this, DateActivity.class);
                startActivityForResult(intent, MainActivityFinal.AGE_REQUEST_CODE);
                overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            }
            if (position == 3) {
                Intent intent = new Intent(MainActivityFinal.this, SoundboardtActivity.class);
                startActivityForResult(intent, MainActivityFinal.SB_REQUEST_CODE);
                overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            }
        }
    }

}

/*
                                    _.--"""--,
                                    .'          `\
  .-""""""-.                      .'              |
 /          '.                   /            .-._/
|             `.                |             |
 \              \          .-._ |          _   \
  `""'-.         \_.-.     \   `          ( \__/
        |             )     '=.       .,   \
       /             (         \     /  \  /
     /`               `\        |   /    `'
     '..-`\        _.-. `\ _.__/   .=.
          |  _    / \  '.-`    `-.'  /
          \_/ |  |   './ _     _  \.'
               '-'    | /       \ |
                      |  .-. .-.  |
                      \ / o| |o \ /
                       |   / \   |
                      / `"`   `"` \
                     /             \
                    | '._.'         \
                    |  /             |
                     \ |             |
                      ||    _    _   /
                      /|\  (_\  /_) /
                      \ \'._  ` '_.'



                        FOR YOU JEFF
 */