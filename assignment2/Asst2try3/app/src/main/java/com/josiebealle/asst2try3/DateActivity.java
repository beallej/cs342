package com.josiebealle.asst2try3;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;


public class DateActivity extends ActionBarActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date);

        Button button = (Button)this.findViewById(R.id.go_button);
        button.setOnClickListener(this);

    }
    @Override
    public void onClick(View v) {
        EditText month = (EditText)this.findViewById(R.id.month);
        String monthString = month.getText().toString();
        System.out.println(monthString);
        EditText day = (EditText)this.findViewById(R.id.day);
        String dayString = day.getText().toString();
        System.out.println(dayString);
        EditText year = (EditText)this.findViewById(R.id.year);
        String yearString = year.getText().toString();
        System.out.println(yearString);

        int curMonth = Calendar.getInstance().get(Calendar.MONTH);
        int curDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        int curYear = Calendar.getInstance().get(Calendar.YEAR);
        System.out.println("current day");
        System.out.println(curDay);
        System.out.println(curMonth);
        System.out.println(curYear);
        int age = 0;

        if (Integer.parseInt(monthString)-1<=curMonth && Integer.parseInt(dayString)<curDay) {
            age = curYear-Integer.parseInt(yearString);
        }
        else {
            age = curYear-Integer.parseInt(yearString)-1;
        }

        int[] months = new int[12];
        months[0] = 31;
        months[1] = 28;
        months[2] = 31;
        months[3] = 30;
        months[4] = 31;
        months[5] = 30;
        months[6] = 31;
        months[7] = 31;
        months[8] = 30;
        months[9] = 31;
        months[10] = 30;
        months[11] = 31;

        String resultMessage = Integer.toString(age);

        if (Integer.parseInt(dayString)>months[Integer.parseInt(monthString)-1] || Integer.parseInt(monthString)>12 || Integer.parseInt(yearString)>curYear) {
            resultMessage = "That's not a real birthday!";
        }

        TextView resultTextView = (TextView)findViewById(R.id.result_text_view);
        resultTextView.setText(resultMessage);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_date, menu);
        return true;
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            return true;

        }

        //back button
        if (id == 16908332) {
            Intent intent = new Intent(this, MainActivityFinal.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

