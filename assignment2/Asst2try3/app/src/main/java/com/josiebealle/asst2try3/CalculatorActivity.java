package com.josiebealle.asst2try3;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class CalculatorActivity extends ActionBarActivity {

    //selected operator
    public String selected = null;

    //+,-,*,/ buttons
    public Button[] operators;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);
        final TextView ans = (TextView)this.findViewById(R.id.answer);
        Button button = (Button)this.findViewById(R.id.goButton);
        View.OnClickListener clickListener = new View.OnClickListener() {
            public void onClick(View v) {
                String answer = solve();
                ans.setText(answer);

            }
        };
        button.setOnClickListener(clickListener);


        Button plusButton = (Button)this.findViewById(R.id.add);
        Button subButton = (Button)this.findViewById(R.id.subtract);
        Button timesButton = (Button)this.findViewById(R.id.times);
        Button divButton = (Button)this.findViewById(R.id.divide);
        operators = new Button[]{plusButton, subButton, timesButton, divButton};
        int p1 = getResources().getColor(R.color.pink1);

        for (int i = 0; i<4; i++){
            Button btn = operators[i];
            btn.setBackgroundColor(p1);
            btn.setOnClickListener(new MyListener(i));

        }
    }

    //http://stackoverflow.com/questions/6663380/is-it-possible-to-write-a-for-loop-to-assign-listeners-to-many-button-with-the-s
    private class MyListener implements Button.OnClickListener {
        int pos;

        //selected is darker pink
        int p1 = getResources().getColor(R.color.pink1);
        int p2 = getResources().getColor(R.color.pink2);
        public MyListener (int position) {
            pos = position;
        }
        @Override
        public void onClick(View v) {
            for (Button b : operators){
                b.setBackgroundColor(p1);
            }
            v.setBackgroundColor(p2);
            Button b = (Button)v;
            selected = (String)b.getText();

        }
    }


    //solves problem
    public String solve(){
        EditText number1 = (EditText)this.findViewById(R.id.number1);
        EditText number2 = (EditText)this.findViewById(R.id.number2);
        String numString1 = number1.getText().toString();
        String numString2 = number2.getText().toString();



        String answer;
        if (selected==null){
            answer = "Choose an operator.";
        }
        else {
            try {
                float num1 = Float.valueOf(numString1);
                float num2 = Float.valueOf(numString2);
                float ans;
                switch (selected) {
                    case "+":
                        ans = num1 + num2;
                        break;
                    case "-":
                        ans = num1 - num2;
                        break;
                    case "×":
                        ans = num1 * num2;
                        break;
                    default:
                        if (num2 == 0) {
                            answer = "Can't divide by 0";
                            return answer;
                        }
                        ans = num1 / num2;

                }
                //integer answer
                if (ans%1.0==0.0){
                    answer = Integer.toString((int)ans);
                }
                else {
                    answer = Float.toString(ans);
                }


            } catch (Exception e) {
                answer = "Only enter numbers.";
            }
        }
        return answer;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_calculator, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            return true;

        }

        //back button
        if (id == 16908332) {
            Intent intent = new Intent(this, MainActivityFinal.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
