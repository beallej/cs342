package com.josiebealle.asst2try3;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.ImageButton;


public class SoundboardtActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soundboardt);

        ImageButton flawlessButton = (ImageButton)this.findViewById(R.id.flawless);
        ImageButton gimmeButton = (ImageButton)this.findViewById(R.id.gimmeSomethin);
        ImageButton jnsqButton = (ImageButton)this.findViewById(R.id.jeNeSaisQuoi);
        ImageButton surfButton = (ImageButton)this.findViewById(R.id.surfboardt);

        flawlessButton.setImageResource(R.drawable.flawless);
        gimmeButton.setImageResource(R.drawable.gimme);
        jnsqButton.setImageResource(R.drawable.jenesaisquoi);
        surfButton.setImageResource(R.drawable.surfboardt);

        View.OnClickListener fclickListener = new View.OnClickListener() {
            public void onClick(View v) {
                MediaPlayer fPlayer = MediaPlayer.create(SoundboardtActivity.this, R.raw.flawless);
                fPlayer.start();
            }
        };
        flawlessButton.setOnClickListener(fclickListener);

        View.OnClickListener gclickListener = new View.OnClickListener() {
            public void onClick(View v) {
                MediaPlayer gPlayer = MediaPlayer.create(SoundboardtActivity.this, R.raw.gimme);
                gPlayer.start();
            }
        };
        gimmeButton.setOnClickListener(gclickListener);

        View.OnClickListener jclickListener = new View.OnClickListener() {
            public void onClick(View v) {
                MediaPlayer jPlayer = MediaPlayer.create(SoundboardtActivity.this, R.raw.jenesaisquoi);
                jPlayer.start();
            }
        };
        jnsqButton.setOnClickListener(jclickListener);

        View.OnClickListener sclickListener = new View.OnClickListener() {
            public void onClick(View v) {
                MediaPlayer sPlayer = MediaPlayer.create(SoundboardtActivity.this, R.raw.surfboard);
                sPlayer.start();
            }
        };
        surfButton.setOnClickListener(sclickListener);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_soundboardt, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            return true;

        }

        //back button
        if (id == 16908332) {
            Intent intent = new Intent(this, MainActivityFinal.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
